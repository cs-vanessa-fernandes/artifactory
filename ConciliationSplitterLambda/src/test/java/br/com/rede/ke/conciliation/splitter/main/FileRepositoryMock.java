/*
 * Copyright 2016 Rede S.A.
 *************************************************************
 * Nome     : FileRepositoryMock.java
 * Descrição: FileRepositoryMock.java.
 * Autor    : Jefferson Brito <jose.josue@userede.com.br>
 * Data     : 16/11/2016
 * Empresa  : Rede
 */
package br.com.rede.ke.conciliation.splitter.main;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.List;

import org.apache.commons.lang.NotImplementedException;

import br.com.rede.ke.conciliation.util.exception.ConciliationRuntimeException;
import br.com.rede.ke.conciliation.util.filerepository.FileRepository;
import br.com.rede.ke.conciliation.util.filerepository.S3File;



/**
 * Created by gustavo.silva on 08/08/2016.
 */
public class FileRepositoryMock implements FileRepository {

    /** The base dir. */
    private File baseDir;

    /**
     * Instantiates a new file repository mock.
     *
     * @param baseDirPath
     *            the base dir path
     */
    public FileRepositoryMock(String baseDirPath) {
        baseDir = new File(baseDirPath);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * com.conciliation.util.filerepository.FileRepository#getSumaryFiles(java.
     * lang.String)
     */
    @Override
    public List<String> getSumaryFiles(String path) throws IOException {
        throw new NotImplementedException("Should not be called in this flow");
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * com.conciliation.util.filerepository.FileRepository#getBufferReader(java.
     * lang.String, java.lang.String, java.lang.String)
     */
    @Override
    public BufferedReader getBufferReader(String bucket, String path, String fileName)
        throws ConciliationRuntimeException {
        return getBufferReader(bucket, path + File.separator + fileName);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * com.conciliation.util.filerepository.FileRepository#getBufferReader(java.
     * lang.String, java.lang.String)
     */
    @Override
    public BufferedReader getBufferReader(String bucketName, String path) throws ConciliationRuntimeException {
        try {
            String fullPath = baseDir.getAbsolutePath() + File.separator + bucketName + File.separator + path;
            File newFile = new File(fullPath);
            return new BufferedReader(new FileReader(newFile));
        } catch (IOException e) {
            throw new ConciliationRuntimeException("Error reading file: " + e.getMessage());
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * com.conciliation.util.filerepository.FileRepository#upLoadFile(java.lang.
     * String, java.io.InputStream, java.lang.String)
     */
    @Override
    public void upLoadFile(String bucketName, InputStream input, String path) throws ConciliationRuntimeException {
        String fullPath = baseDir.getAbsolutePath() + File.separator + bucketName + File.separator + path;
        File newFile = new File(fullPath);

        try {
            Files.copy(input, newFile.toPath(), StandardCopyOption.REPLACE_EXISTING);
        } catch (IOException e) {
            throw new ConciliationRuntimeException(e);
        }
    }

    @Override
    public void uploadFile(String bucketName, File file, String filename) throws ConciliationRuntimeException {
        String fullPath = baseDir.getAbsolutePath() + File.separator + bucketName + File.separator + filename;
        File newFile = new File(fullPath);

        try {
            Files.copy(file.toPath(), newFile.toPath(), StandardCopyOption.REPLACE_EXISTING);
        } catch (IOException e) {
            throw new ConciliationRuntimeException(e);
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * com.conciliation.util.filerepository.FileRepository#cutFile(java.lang.
     * String, java.lang.String, java.lang.String, java.lang.String)
     */
    @Override
    public void cutFile(String bucketNameSource, String pathOrig, String bucketNameDestiny, String pathDestino)
        throws ConciliationRuntimeException {
        throw new NotImplementedException("Should not be called in this flow");
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * com.conciliation.util.filerepository.FileRepository#copyFile(java.lang.
     * String, java.lang.String, java.lang.String, java.lang.String)
     */
    @Override
    public void copyFile(String bucketNameSource, String pathOrig, String bucketNameDestiny, String pathDestino)
        throws ConciliationRuntimeException {
        throw new NotImplementedException("Should not be called in this flow");
    }

    /*
     * (non-Javadoc)
     *
     * @see com.conciliation.util.filerepository.FileRepository#moveProcessed(
     * boolean, com.conciliation.util.filerepository.S3File)
     */
    @Override
    public void moveProcessed(boolean ok, S3File file) throws ConciliationRuntimeException {
        throw new NotImplementedException("Should not be called in this flow");
    }
}