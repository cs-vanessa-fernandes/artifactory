/*
 * Copyright 2016 Rede S.A.
 *************************************************************
 * Nome     : RunSplitterLog.java
 * Descrição: RunSplitterLog.java.
 * Autor    : Renan Furtado <renan.furtado@iteris.com.br>
 * Data     : 07/12/2016
 * Empresa  : Iteris
 ************************MODIFICAÇÕES************************
 *Autor    : Renan Del Puppo Furtado
 *Data     : 11/01/2016
 *Empresa  : Iteris
 *Descrição: Ajuste de checkstyle
 *ID       : AM 185248
 *************************************************************
 *Autor    : Thiago Almeida
 *Data     : 06/02/2017
 *Empresa  : Iteris
 *Descrição: Adicionando variáveis de ambiente
 *ID       : 188840
 *************************************************************
 */
package br.com.rede.ke.conciliation.splitter.main;

import static net.logstash.logback.argument.StructuredArguments.value;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.com.rede.ke.conciliation.splitter.config.ConfigEnvironment;
import br.com.rede.ke.conciliation.splitter.exception.ConciliationSplitterEngineException;
import br.com.rede.ke.conciliation.splitter.lambda.handler.SplitterHandler;
import br.com.rede.ke.conciliation.splitter.log.TagEnum;
import br.com.rede.ke.conciliation.util.enums.LogSource;
import br.com.rede.ke.conciliation.util.filerepository.FileRepository;
import br.com.rede.ke.conciliation.util.log.LogProcessingBuilder;
import br.com.rede.ke.conciliation.util.log.LogProcessingBuilder.LogStatus;
import ch.qos.logback.classic.LoggerContext;
import net.logstash.logback.argument.StructuredArgument;


/**
 * Created by gustavo.silva on 08/08/2016.
 * Updated by renan.furrtado on 11/10/2016
 */
public class RunSplitterLog {

    /** The user home. */
    private String userHome;

    /** The test folder. */
    private Path testFolder;

    /** The log builder. */
    private LogProcessingBuilder logBuilder;

    /** The config repository mock. */
    private ConfigEnvironment configRepositoryMock;

    /** The file repository mock. */
    private FileRepository fileRepositoryMock;

    /** The orchestrator. */
    private SplitterOrchestrator orchestrator;

    /**
     * Setup
     */
    @Before
    public void setup(){

        this.userHome = System.getProperty("user.home");
        this.testFolder = Paths.get(userHome + File.separator + "buckets");
        this.configRepositoryMock = new ConfigEnvironmentMock();
        this.fileRepositoryMock = new FileRepositoryMock(testFolder.toString());

        this.orchestrator = new SplitterOrchestrator(fileRepositoryMock, configRepositoryMock);
    }

    /**
     * Run spliter.
     *
     * @throws IOException Signals that an I/O exception has occurred.
     */
    @Test
    public void runSpliter() throws IOException{
        Path splitterDir = Paths.get(testFolder + "/splitter");
        Files.walkFileTree(splitterDir, new ExtratoFileVisitor(splitterDir));
        ((LoggerContext)LoggerFactory.getILoggerFactory()).stop();//Faz o flush e encerra o logback
    }

    /**
     * The Class ExtratoFileVisitor.
     */
    public class ExtratoFileVisitor extends SimpleFileVisitor<Path>{

        /** The kibana logger. */
        private Logger kibanaLogger = LoggerFactory.getLogger(SplitterHandler.class);

        /** The splitter dir. */
        private Path splitterDir;

        /**
         * Instantiates a new extrato file visitor.
         *
         * @param splitterDir the splitter dir
         */
        public ExtratoFileVisitor(Path splitterDir) {
            super();
            this.splitterDir = splitterDir;
        }

        /* (non-Javadoc)
         * @see java.nio.file.SimpleFileVisitor#visitFile(java.lang.Object, java.nio.file.attribute.BasicFileAttributes)
         */
        @Override
        public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {

            if(file.getFileName().startsWith(".DS")) {
                return super.visitFile(file, attrs);
            }

            logBuilder = new LogProcessingBuilder();

            logBuilder.addSource(LogSource.SPLITTER);
            logBuilder.description(String.format("Input file: %s", file.toString()));

            try {
                Path filePath = this.splitterDir.relativize(file);
                filePath = Paths.get(splitterDir.getFileName().toString(), filePath.toString());

                this.execute(file, filePath);

            } catch(ConciliationSplitterEngineException e) {
                logBuilder.problem(e.getMessage(), new Throwable(e));
            }

            return super.visitFile(file, attrs);
        }



        /**
         * Execute.
         *
         * @param file the file
         * @param filePath the file path
         * @throws ConciliationSplitterEngineException the conciliation splitter engine exception
         */
        private void execute(Path file, Path filePath) throws ConciliationSplitterEngineException{

            try {

                logBuilder.addSource(LogSource.SPLITTER);

                orchestrator.processFile(file.getFileName().toString(),
                    filePath.getParent().toString(), logBuilder);


            } catch (ConciliationSplitterEngineException e) {
                StringWriter errors = new StringWriter();
                e.printStackTrace(new PrintWriter(errors));
                logBuilder.problem(e.getMessage(), new Throwable(e));
            }



            String fileName = file.getFileName().toString().trim();
            List<StructuredArgument> values = prepareKibanaLogger(fileName);

            if(logBuilder.getStatus() == LogStatus.SUCCESS){
                kibanaLogger.info("SPLITTER {}", values.toArray());

            }else{
                values.add(value(TagEnum.ERROR_DETAIL.name(), logBuilder.getLastException().toString()));
                kibanaLogger.info("SPLITTER {}", values.toArray());
            }
        }

        private List<StructuredArgument> prepareKibanaLogger(String fileName) {
            List<StructuredArgument> values = new ArrayList<>();
            values.add(value(TagEnum.STATUS.name(), logBuilder.getStatus()));
            values.add(value(TagEnum.SPLITED_FILES_QUANTITY.name(), logBuilder.getSplitedFilesQuantity()));
            values.add(value(TagEnum.FILE_TYPE_NAME.name(), logBuilder.getFileTypeName()));
            values.add(value(TagEnum.FILE_NAME_INPUT.name(), fileName));
            values.add(value(TagEnum.FILE_NAME_OUTPUT.name(), logBuilder.getGenerateFilesNames().toArray()));
            return values;
        }

    }
}