/*
 * Copyright 2016 Rede S.A.
 *************************************************************
 * Nome     : RunSplitter.java
 * Descrição: RunSplitter.java.
 * Autor    : Jefferson Brito <jose.josue@userede.com.br>
 * Data     : 19/12/2016
 * Empresa  : Rede
 ************************MODIFICAÇÕES************************
 *Autor    : Renan Del Puppo Furtado
 *Data     : 11/01/2016
 *Empresa  : Iteris
 *Descrição: Ajuste de checkstyle
 *ID       : AM 185248
 * ************************MODIFICAÇÕES************************
 *Autor    : Thiago Almeida
 *Data     : 06/02/2017
 *Empresa  : Iteris
 *Descrição: Adicionando variáveis de ambiente
 *ID       : 188840
 *************************************************************
 */
package br.com.rede.ke.conciliation.splitter.main;

import java.io.File;
import java.util.logging.Level;
import java.util.logging.Logger;

import br.com.rede.ke.conciliation.splitter.config.ConfigEnvironment;
import br.com.rede.ke.conciliation.splitter.engine.FileSplitter;
import br.com.rede.ke.conciliation.util.filerepository.FileRepository;
import br.com.rede.ke.conciliation.util.log.LogProcessingBuilder;

/**
 * Created by gustavo.silva on 08/08/2016.
 */
public class RunSplitter {
    /** The Constant LOGGER. */
    private static final Logger LOGGER = Logger.getLogger(RunSplitter.class.getName());

    /**
     * The main method.
     *
     * @param args
     *            the arguments
     * @throws Exception
     *             the exception
     */
    public static void main(String[] args) throws Exception {
        String userHome = System.getProperty("user.home");

        String testFolder = userHome + File.separator + "buckets";

        ConfigEnvironment configRepositoryMock = new ConfigEnvironmentMock();
        FileRepository fileRepositoryMock = new FileRepositoryMock(testFolder);

        File dir = new File(testFolder + "/splitter");
        File[] files = dir.listFiles();

        double start = System.currentTimeMillis();
        for (File file : files) {
            if (file.isDirectory() || file.getName().startsWith(".DS")) {
                continue;
            }

            FileSplitter.
            getInstance(configRepositoryMock, fileRepositoryMock, "splitter", file.getName(), new LogProcessingBuilder()).split();
        }
        double end = System.currentTimeMillis();

        LOGGER.log(Level.INFO, "Time elapsed " + ((end - start) / 1000.0) + "s");
        printMemory();
    }

    /**
     * Prints the memory.
     */
    private static void printMemory() {
        int mb = 1024 * 1024;
        Runtime runtime = Runtime.getRuntime();

        LOGGER.log(Level.INFO, "##### Heap utilization statistics [MB] #####");

        // Print used memory
        LOGGER.log(Level.INFO, "Used Memory:" + (runtime.totalMemory() - runtime.freeMemory()) / mb);

        // Print free memory
        LOGGER.log(Level.INFO, "Free Memory:" + runtime.freeMemory() / mb);

        // Print total available memory
        LOGGER.log(Level.INFO, "Total Memory:" + runtime.totalMemory() / mb);

        // Print Maximum available memory
        LOGGER.log(Level.INFO, "Max Memory:" + runtime.maxMemory() / mb);
    }
}