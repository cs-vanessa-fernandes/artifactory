/*
 * Copyright 2017 Rede S.A.
 *************************************************************
 * Nome     : ConfigEnvironmentMock.java
 * Descrição: ConfigEnvironmentMock.java.
 * Autor    : Thiago Boldrin de Almeida <thiago.almeida@iteris.com.br>
 * Data     : 07/02/2017
 * Empresa  : Iteris
 */
package br.com.rede.ke.conciliation.splitter.main;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import br.com.rede.ke.conciliation.splitter.config.ConfigEnvironment;

/**
 * Created by gustavo.silva on 08/08/2016.
 */
public class ConfigEnvironmentMock implements ConfigEnvironment {

    /** The properties. */
    private Properties properties;

    /**
     * Instantiates a new config repository mock.
     */
    public ConfigEnvironmentMock() {
        properties = new Properties();
        InputStream resourceAsStream = getClass().getResourceAsStream("/conciliation-splitter.properties");
        try {
            properties.load(resourceAsStream);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                resourceAsStream.close();
            } catch (IOException ignored) {
            }
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * br.com.rede.ke.conciliation.splitter.config.ConfigRepository#getProperty(
     * java.lang.String)
     */
    @Override
    public String getProperty(String key) {
        return properties.getProperty(key);
    }
}
