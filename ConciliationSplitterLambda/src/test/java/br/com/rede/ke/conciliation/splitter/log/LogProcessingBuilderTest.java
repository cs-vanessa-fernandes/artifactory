/*
 * Copyright 2016 Rede S.A.
 *************************************************************
 * Nome     : LogProcessingBuilderTest.java
 * Descrição: LogProcessingBuilderTest.java.
 * Autor    : Renan Furtado <renan.furtado@iteris.com.br>
 * Data     : 07/12/2016
 * Empresa  : Iteris
 */
package br.com.rede.ke.conciliation.splitter.log;


import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;

import com.fasterxml.jackson.core.JsonProcessingException;

import br.com.rede.ke.conciliation.util.log.LogProcessingBuilder;

import org.junit.Assert;

/**
 * The Class LogProcessingBuilderTest.
 */
public class LogProcessingBuilderTest {

    @InjectMocks
    private LogProcessingBuilder logProcessingBuilder = new LogProcessingBuilder();

    @Before
    public void setup(){
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void getGenerateFilesNamesJson() throws JsonProcessingException{
        this.logProcessingBuilder.addGenerateFileName("file_1.txt");
        this.logProcessingBuilder.addGenerateFileName("file_2.txt");
        this.logProcessingBuilder.addGenerateFileName("file_3.txt");
        this.logProcessingBuilder.addGenerateFileName("file_4.txt");
        this.logProcessingBuilder.addGenerateFileName("file_5.txt");

        String generateFilesNamesJson = this.logProcessingBuilder.getGenerateFilesNamesJson();

        Assert.assertEquals("[ \"file_1.txt\", \"file_2.txt\", \"file_3.txt\", \"file_4.txt\", \"file_5.txt\" ]"
            ,generateFilesNamesJson);
    }
}
