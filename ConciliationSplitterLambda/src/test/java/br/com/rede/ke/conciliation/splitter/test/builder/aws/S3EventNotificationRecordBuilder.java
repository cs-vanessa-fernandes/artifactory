/*
 * Copyright 2016 Rede S.A.
 *************************************************************
 * Nome     : S3EventNotificationRecordBuilder.java
 * Descrição: S3EventNotificationRecordBuilder.java.
 * Autor    : Renan Furtado <renan.furtado@iteris.com.br>
 * Data     : 07/12/2016
 * Empresa  : Iteris
 */
package br.com.rede.ke.conciliation.splitter.test.builder.aws;

import com.amazonaws.services.s3.event.S3EventNotification.S3EventNotificationRecord;

/**
 * The Class S3EventNotificationRecordBuilder.
 */
public class S3EventNotificationRecordBuilder {


    /**
     * Gets the s 3 event notification record.
     *
     * @return the s 3 event notification record
     */
    public static S3EventNotificationRecord getS3EventNotificationRecord(){
        S3EventNotificationRecord s3e = new S3EventNotificationRecord("Brasil", //awsRegion
            "", //eventName
            "", //eventSource
            "", //eventTime
            "", //eventVersion
            null, //requestParameters
            null, //responseElements
            null, //s3
            null); //user Identity


        return s3e;

    }

}
