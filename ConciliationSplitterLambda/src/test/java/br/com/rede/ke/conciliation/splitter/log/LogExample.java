/*
 * Copyright 2016 Rede S.A.
 *************************************************************
 * Nome     : LogExample.java
 * Descrição: LogExample.java.
 * Autor    : Renan Furtado <renan.furtado@iteris.com.br>
 * Data     : 07/12/2016
 * Empresa  : Iteris
 */
package br.com.rede.ke.conciliation.splitter.log;

import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.qos.logback.classic.LoggerContext;

import static net.logstash.logback.argument.StructuredArguments.value;

public class LogExample {

    private Logger logger;

    @Before
    public void setup(){
        logger = LoggerFactory.getLogger(LogExample.class);
    }

    /**
     * Test.
     */
    @Test
    public void test(){
        int i = 10;
        logger.info("Teste {}: IP {}", value("iteration", i), value("IP", "127.0.0.1"));
        LoggerContext loggerContext = (LoggerContext) LoggerFactory.getILoggerFactory();
        loggerContext.stop();
    }


}
