/*
 * Copyright 2016 Rede S.A.
 *************************************************************
 * Nome     : SeparationTypeEnum.java
 * Descrição: SeparationTypeEnum.java.
 * Autor    : Jefferson Brito <jose.josue@userede.com.br>
 * Data     : 16/11/2016
 * Empresa  : Rede
 */
package br.com.rede.ke.conciliation.splitter.engine;

/**
 * Created by gustavo.silva on 10/08/2016.
 */
public enum SeparationTypeEnum {

    /** The delimited. */
    DELIMITED,

    /** The positional. */
    POSITIONAL
}
