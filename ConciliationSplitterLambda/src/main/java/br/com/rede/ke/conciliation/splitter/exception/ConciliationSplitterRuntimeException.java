/*
 * Copyright 2016 Rede S.A.
 *************************************************************
 * Nome     : ConciliationSplitterRuntimeException.java
 * Descrição: ConciliationSplitterRuntimeException.java.
 * Autor    : Jefferson Brito <jose.josue@userede.com.br>
 * Data     : 16/12/2016
 * Empresa  : Rede
 */
package br.com.rede.ke.conciliation.splitter.exception;

/**
 * The Class ConciliationSplitterRuntimeException.
 */
public class ConciliationSplitterRuntimeException extends RuntimeException {
    /**
     *
     */
    private static final long serialVersionUID = -7805479508978931729L;

    /**
     * Instantiates a new conciliation splitter runtime exception.
     *
     * @param messageError
     *            the message error
     */
    public ConciliationSplitterRuntimeException(String messageError) {
        super(messageError);
    }

    /**
     * Instantiates a new conciliation splitter runtime exception.
     *
     * @param e
     *            the e
     */
    public ConciliationSplitterRuntimeException(Throwable e) {
        super(e);
    }

    /**
     * Instantiates a new conciliation splitter runtime exception.
     *
     * @param message
     *            the message
     * @param cause
     *            the cause
     */
    public ConciliationSplitterRuntimeException(String message, Throwable cause) {
        super(message, cause);
    }
}