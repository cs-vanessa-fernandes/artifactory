/*
 * Copyright 2016 Rede S.A.
 *************************************************************
 * Nome     : SplitterOrchestrator.java
 * Descrição: SplitterOrchestrator.java.
 * Autor    : Jefferson Brito <jose.josue@userede.com.br>
 * Data     : 16/11/2016
 * Empresa  : Rede
 ************************MODIFICAÇÕES************************
 *Autor    : Renan Del Puppo Furtado
 *Data     : 11/01/2016
 *Empresa  : Iteris
 *Descrição: Ajuste de checkstyle
 *ID       : AM 185248
 *************************************************************
 *Autor    : Thiago Almeida
 *Data     : 06/02/2017
 *Empresa  : Iteris
 *Descrição: Adicionando variáveis de ambiente
 *ID       : 188840
 *************************************************************
 */

package br.com.rede.ke.conciliation.splitter.main;

import br.com.rede.ke.conciliation.splitter.config.ConfigEnvironment;
import br.com.rede.ke.conciliation.splitter.engine.FileSplitter;
import br.com.rede.ke.conciliation.splitter.exception.ConciliationSplitterEngineException;
import br.com.rede.ke.conciliation.util.filerepository.FileRepository;
import br.com.rede.ke.conciliation.util.log.LogProcessingBuilder;

/**
 * The Class SplitterOrchestrator.
 */
public class SplitterOrchestrator {

    /** The file repository. */
    private FileRepository fileRepository;

    /** The config repository. */
    private ConfigEnvironment configRepository;

    /**
     * Instantiates a new splitter orchestrator.
     *
     * @param fileRepository
     *            the file repository
     * @param configRepository
     *            the config repository
     */
    public SplitterOrchestrator(FileRepository fileRepository, ConfigEnvironment configRepository) {
        this.fileRepository = fileRepository;
        this.configRepository = configRepository;
    }

    /**
     * Process file.
     *
     * @param filePath
     *            the file path
     * @param bucket
     *            the bucket
     * @param logBuilder
     *            the log builder
     * @throws ConciliationSplitterEngineException
     *             the conciliation splitter engine exception
     */
    public void processFile(String filePath, String bucket, LogProcessingBuilder logBuilder)
        throws ConciliationSplitterEngineException {

        FileSplitter.getInstance(configRepository, fileRepository, bucket, filePath, logBuilder).split();
    }
}
