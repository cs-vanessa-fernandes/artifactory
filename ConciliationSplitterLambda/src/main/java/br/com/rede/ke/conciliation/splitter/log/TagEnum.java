/*
 * Copyright 2016 Rede S.A.
 *************************************************************
 * Nome     : TagEnum.java
 * Descrição: TagEnum.java.
 * Autor    : Renan Furtado <renan.furtado@iteris.com.br>
 * Data     : 07/12/2016
 * Empresa  : Iteris
 */
package br.com.rede.ke.conciliation.splitter.log;

/**
 * The Enum TagEnum.
 */
public enum TagEnum {

    /** The status. */
    STATUS,

    /** The file type name. */
    FILE_TYPE_NAME,

    /** The file name input. */
    FILE_NAME_INPUT,

    /** The file name output. */
    FILE_NAME_OUTPUT,

    /** The error detail. */
    ERROR_DETAIL,

    /** The quantity files splited . */
    SPLITED_FILES_QUANTITY;

}
