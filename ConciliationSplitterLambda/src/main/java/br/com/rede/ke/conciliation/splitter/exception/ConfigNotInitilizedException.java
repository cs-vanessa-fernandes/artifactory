/*
 * Copyright 2016 Rede S.A.
 *************************************************************
 * Nome     : ConfigNotInitilizedException.java
 * Descrição: ConfigNotInitilizedException.java.
 * Autor    : Jefferson Brito <jose.josue@userede.com.br>
 * Data     : 16/11/2016
 * Empresa  : Rede
 */
package br.com.rede.ke.conciliation.splitter.exception;

/**
 * The Class ConfigNotInitilizedException.
 */
public class ConfigNotInitilizedException extends RuntimeException {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 1356160488207514661L;

}
