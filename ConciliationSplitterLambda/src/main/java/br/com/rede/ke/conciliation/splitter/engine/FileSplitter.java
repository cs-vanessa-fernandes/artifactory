/*
 * Copyright 2016 Rede S.A.
 *************************************************************
 * Nome     : FileSplitter.java
 * Descrição: FileSplitter.java.
 * Autor    : Jefferson Brito <jose.josue@userede.com.br>
 * Data     : 20/12/2016
 * Empresa  : Rede
 ************************MODIFICAÇÕES************************
 *Autor    : Renan Del Puppo Furtado
 *Data     : 11/01/2016
 *Empresa  : Iteris
 *Descrição: Ajuste de checkstyle
 *ID       : AM 185248
 *************************************************************
 *Autor    : Thiago Almeida
 *Data     : 06/02/2017
 *Empresa  : Iteris
 *Descrição: Adicionando variáveis de ambiente
 *ID       : 188840
 *************************************************************
 */
package br.com.rede.ke.conciliation.splitter.engine;

import static br.com.rede.ke.conciliation.splitter.engine.SeparationTypeEnum.DELIMITED;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.StandardOpenOption;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.FilenameUtils;

import br.com.rede.ke.conciliation.splitter.config.ConfigEnvironment;
import br.com.rede.ke.conciliation.splitter.exception.ConciliationSplitterEngineException;
import br.com.rede.ke.conciliation.splitter.exception.ConciliationSplitterRuntimeException;
import br.com.rede.ke.conciliation.util.exception.ConciliationRuntimeException;
import br.com.rede.ke.conciliation.util.filerepository.FileRepository;
import br.com.rede.ke.conciliation.util.log.LogProcessingBuilder;

/**
 * Created by gustavo.silva on 08/08/2016.
 */
public final class FileSplitter {
    /** The log builder. */
    private LogProcessingBuilder logBuilder;

    /** The header config. */
    private String headerConfig;

    /** The trailer config. */
    private String trailerConfig;

    /** The separator. */
    private String separator;

    /** The source bucket. */
    private String sourceBucket;

    /** The file name. */
    private String fileName;

    /** The target bucket. */
    private String targetBucket;

    /** The file repository. */
    private FileRepository fileRepository;

    /** The simple file name. */
    private String simpleFileName;

    /** The extension. */
    private String extension;

    /** The index. */
    private int index = 1;

    /**
     * Instantiates a new file splitter.
     *
     * @param configRepository
     *            the config repository
     * @param fileRepository
     *            the file repository
     * @param fileTypeName
     *            the file type name
     * @param sourceBucket
     *            the source bucket
     * @param fileName
     *            the file name
     * @param logBuilder
     */
    private FileSplitter(ConfigEnvironment configRepository, FileRepository fileRepository, String fileTypeName,
        String sourceBucket, String fileName, LogProcessingBuilder logBuilder) {
        this.logBuilder = logBuilder;
        headerConfig = configRepository.getProperty("FILETYPE_" + fileTypeName + "_VALIDATION_HEADER");
        trailerConfig = configRepository.getProperty("FILETYPE_" + fileTypeName + "_VALIDATION_TRAILER");
        separator = configRepository.getProperty("FILETYPE_" + fileTypeName + "_SEPARATOR");
        targetBucket = configRepository.getProperty("BUCKET_TARGET");
        this.sourceBucket = sourceBucket;
        this.fileName = fileName;
        simpleFileName = FilenameUtils.getName(fileName);
        extension = FilenameUtils.getExtension(fileName);

        if (extension != null && !extension.isEmpty()) {
            extension = FilenameUtils.EXTENSION_SEPARATOR_STR + extension;
        }

        this.fileRepository = fileRepository;

    }

    /**
     * Gets the single instance of FileSplitter.
     *
     * @param configRepository
     *            the config repository
     * @param fileRepository
     *            the file repository
     * @param bucket
     *            the bucket
     * @param fileName
     *            the file name
     * @param logBuilder the logBuilder
     * @return single instance of FileSplitter
     * @throws ConciliationSplitterEngineException
     *             the conciliation splitter engine exception
     */
    public static FileSplitter getInstance(ConfigEnvironment configRepository, FileRepository fileRepository,
        String bucket, String fileName, LogProcessingBuilder logBuilder) throws ConciliationSplitterEngineException {

        try (BufferedReader reader = fileRepository.getBufferReader(bucket, fileName)) {
            String firstLine = reader.readLine();
            String fileType = null;
            String[] fileTypes = configRepository.getProperty("FILE_TYPES").split(";");

            for (String type : fileTypes) {
                String fileConfig = configRepository.getProperty("FILETYPE_" + type + "_VALIDATION_FILE");
                String separator = configRepository.getProperty("FILETYPE_" + type + "_SEPARATOR");
                if (checkLine(firstLine, fileConfig, separator)) {
                    fileType = type;
                    break;
                }
            }

            return new FileSplitter(configRepository, fileRepository, fileType, bucket, fileName, logBuilder);
        } catch (IOException e) {
            throw new ConciliationSplitterEngineException(e);
        }
    }

    /**
     * Split.
     *
     * @throws ConciliationSplitterEngineException
     *             the conciliation splitter engine exception
     */
    public void split() throws ConciliationSplitterEngineException {
        try (BufferedReader bufferReader = fileRepository.getBufferReader(sourceBucket, fileName)) {
            List<ProcessedFile> files = bufferReader.lines().collect(this::newCollector, this::accept, this::combine);

            this.setLogInfos(files);

            checkIntegrity(files);
            uploadFiles(files);
        } catch (IOException | ConciliationSplitterEngineException e) {
            throw new ConciliationSplitterEngineException(e);
        }
    }

    private void setLogInfos(List<ProcessedFile> files) {
        this.logBuilder.setSplitedFilesQuantity(files.size());
        for (ProcessedFile pf : files) {
            this.logBuilder.addGenerateFileName(pf.fullFileName);
        }
    }

    /**
     * Check integrity.
     *
     * @param files
     *            the files
     * @throws ConciliationSplitterEngineException
     *             the conciliation splitter engine exception
     */
    public void checkIntegrity(List<ProcessedFile> files) throws ConciliationSplitterEngineException {
        long headers = files.stream().filter(p -> p.hasHeader).count();
        long trailers = files.stream().filter(p -> p.hasTrailer).count();

        if (headers != trailers) {
            deleteTempFiles(files);

            throw new ConciliationSplitterEngineException(String.format(
                "File %s in bucket %s is invalid. Headers %d, Trailers %d", fileName, sourceBucket, headers, trailers));
        }
    }

    /**
     * Upload files.
     *
     * @param files
     *            the files
     * @throws ConciliationSplitterEngineException
     *             the conciliation splitter engine exception
     */
    public void uploadFiles(List<ProcessedFile> files) throws ConciliationSplitterEngineException {
        try {
            for (ProcessedFile file : files) {
                if (file.hasHeader && file.hasTrailer) {
                    // InputStream is =
                    // Files.newInputStream(file.tempFile.toPath(),
                    // StandardOpenOption.READ);
                    File temp = new File(file.tempFile.toString());
                    fileRepository.uploadFile(targetBucket, temp, file.fullFileName);
                    index++;
                    temp = null;
                }

                file.tempFile.delete();
            }
        } catch (ConciliationRuntimeException e) {
            throw new ConciliationSplitterEngineException(e);
        }
    }

    /**
     * New collector.
     *
     * @return the list
     */
    private List<ProcessedFile> newCollector() {
        return new ArrayList<>();
    }

    /**
     * Combine.
     *
     * @param r
     *            the r
     * @param r1
     *            the r 1
     */
    private void combine(List<ProcessedFile> r, List<ProcessedFile> r1) {
    }

    /**
     * Delete temp files.
     *
     * @param files
     *            the files
     */
    private void deleteTempFiles(List<ProcessedFile> files) {
        for (ProcessedFile file : files) {
            file.tempFile.delete();
        }
    }

    /**
     * Accept.
     *
     * @param files
     *            the files
     * @param line
     *            the line
     */
    private void accept(List<ProcessedFile> files, String line) {
        try {
            ProcessedFile processedFile;
            if (files.isEmpty()) {
                processedFile = new ProcessedFile(simpleFileName, index++, extension);
                files.add(processedFile);
            } else {
                processedFile = files.get(files.size() - 1);
            }

            if (checkLine(line, headerConfig, separator)) {
                processedFile.hasHeader = true;
            }

            if (checkLine(line, trailerConfig, separator)) {
                processedFile.hasTrailer = true;
                files.add(new ProcessedFile(simpleFileName, index++, extension));
            }

            processedFile.append(line).append(System.lineSeparator());
            if (processedFile.hasTrailer) {
                processedFile.closeFile();
            }

        } catch (IOException e) {
            throw new ConciliationSplitterRuntimeException(e);
        }
    }

    /**
     * Check line.
     *
     * @param currentLine
     *            the current line
     * @param configurationValue
     *            the configuration value
     * @param separator
     *            the separator
     * @return true, if successful
     */
    private static boolean checkLine(String currentLine, String configurationValue, String separator) {

        String[] configurationParts = configurationValue.split(";");
        String expectedText;
        String actualText;
        String operatorStr = configurationParts[0];
        String separationTypeStr = configurationParts[1];

        OperatorTypeEnum operatorType = OperatorTypeEnum.valueOf(operatorStr);
        SeparationTypeEnum separationType = SeparationTypeEnum.valueOf(separationTypeStr);

        if (separationType == DELIMITED) {
            String[] lineParts = currentLine.split(separator);
            Integer column = Integer.parseInt(configurationParts[2]) - 1;
            expectedText = configurationParts[3];
            actualText = lineParts[column];

        } else {
            Integer start = Integer.parseInt(configurationParts[2]) - 1;
            Integer end = Integer.parseInt(configurationParts[3]);
            expectedText = configurationParts[4];

            if (currentLine.length() > end) {
                actualText = currentLine.substring(start, end);
            } else {
                actualText = currentLine.substring(start, currentLine.length() - 1);
            }
        }

        switch (operatorType) {
            case EQUALS:
                return actualText.equalsIgnoreCase(expectedText);
            case CONTAINS:
                return actualText.contains(expectedText);
            case NOT_EQUALS:
                return !actualText.equalsIgnoreCase(expectedText);
            default:
        }

        return false;
    }

    /**
     * Gets the log builder.
     *
     * @return the logBuilder
     */
    public LogProcessingBuilder getLogBuilder() {
        return logBuilder;
    }

    /**
     * The Class ProcessedFile.
     */
    public static class ProcessedFile {

        /** The temp file. */
        private File tempFile;

        /** The has header. */
        private boolean hasHeader;

        /** The has trailer. */
        private boolean hasTrailer;

        /** The full file name. */
        private String fullFileName;

        /** The buffered content. */
        private StringBuilder bufferedContent = new StringBuilder();

        /**
         * Instantiates a new processed file.
         *
         * @param fileName
         *            the file name
         * @param index
         *            the index
         * @param extension
         *            the extension
         * @throws IOException
         *             Signals that an I/O exception has occurred.
         */
        public ProcessedFile(String fileName, Integer index, String extension) throws IOException {
            String newFileName = MessageFormat.format("{0}-{1,number,0000}", fileName, index);
            tempFile = File.createTempFile(newFileName, extension);
            fullFileName = newFileName + extension;
        }

        /**
         * Append.
         *
         * @param sequence
         *            the sequence
         * @return the processed file
         * @throws IOException
         *             Signals that an I/O exception has occurred.
         */
        public ProcessedFile append(String sequence) throws IOException {
            bufferedContent.append(sequence);
            if (bufferedContent.length() > 524288) {
                Files.write(tempFile.toPath(), bufferedContent.toString().getBytes(), StandardOpenOption.APPEND);
                bufferedContent = new StringBuilder();
            }
            return this;
        }

        /**
         * Close file.
         *
         * @throws IOException
         *             Signals that an I/O exception has occurred.
         */
        public void closeFile() throws IOException {
            Files.write(tempFile.toPath(), bufferedContent.toString().getBytes(), StandardOpenOption.APPEND);
            bufferedContent = null;
        }
    }
}