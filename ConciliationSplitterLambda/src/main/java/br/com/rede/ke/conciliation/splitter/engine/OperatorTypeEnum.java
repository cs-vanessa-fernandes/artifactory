/*
 * Copyright 2016 Rede S.A.
 *************************************************************
 * Nome     : OperatorTypeEnum.java
 * Descrição: OperatorTypeEnum.java.
 * Autor    : Jefferson Brito <jose.josue@userede.com.br>
 * Data     : 16/11/2016
 * Empresa  : Rede
 */
package br.com.rede.ke.conciliation.splitter.engine;

/**
 * The Enum OperatorTypeEnum.
 */
public enum OperatorTypeEnum {

    /** The equals. */
    EQUALS,

    /** The contains. */
    CONTAINS,

    /** The not equals. */
    NOT_EQUALS
}
