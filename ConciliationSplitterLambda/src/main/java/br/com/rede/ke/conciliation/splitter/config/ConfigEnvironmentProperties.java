/*
 * Copyright 2017 Rede S.A.
 *************************************************************
 * Nome     : ConfigEnvironmentProperties.java
 * Descrição: ConfigEnvironmentProperties.java.
 * Autor    : Thiago Boldrin de Almeida <thiago.almeida@iteris.com.br>
 * Data     : 07/02/2017
 * Empresa  : Iteris
 */
package br.com.rede.ke.conciliation.splitter.config;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;



/**
 * The Class ConfigRepositoryS3.
 */
public final class ConfigEnvironmentProperties implements ConfigEnvironment {

    /** The instance. */
    private static ConfigEnvironmentProperties instance;

    /** The properties. */
    private static Properties properties;

    /**
     * Instantiates a new config repository S 3.
     *
     * @param alias the alias
     */
    public ConfigEnvironmentProperties() {
        properties = new Properties();
        InputStream resourceAsStream = getClass().getResourceAsStream("/conciliation-splitter.properties");
        try {
            properties.load(resourceAsStream);
            properties.putAll(System.getenv());
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                resourceAsStream.close();
            } catch (IOException ignored) {
            }
        }
    }

    /**
     * Gets the single instance of ConfigRepositoryS3.
     *
     * @param alias the alias
     * @return single instance of ConfigRepositoryS3
     */
    public static synchronized ConfigEnvironmentProperties getInstance() {
        if (instance == null) {
            instance = new ConfigEnvironmentProperties();
        }
        return instance;
    }

    /* (non-Javadoc)
     * @see br.com.rede.ke.conciliation.splitter.config.ConfigRepository#getProperty(java.lang.String)
     */
    @Override
    public String getProperty(String key) {
        return properties.getProperty(key);
    }

}
