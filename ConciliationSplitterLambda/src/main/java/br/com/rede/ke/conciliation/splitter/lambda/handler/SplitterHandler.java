/*
 * Copyright 2016 Rede S.A.
 *************************************************************
 * Nome     : SplitterHandler.java
 * Descrição: SplitterHandler.java.
 * Autor    : Jefferson Brito <jose.josue@userede.com.br>
 * Data     : 19/12/2016
 * Empresa  : Rede
 **************************MODIFICAÇÕES***********************
 *Autor    : Renan Del Puppo Furtado
 *Data     : 27/01/2017
 *Empresa  : Iteris
 *Descrição: Ajuste de captura de erros
 *ID       : AM 187.371
 ************************MODIFICAÇÕES************************
 *Autor    : Renan Del Puppo Furtado
 *Data     : 11/01/2016
 *Empresa  : Iteris
 *Descrição: Ajuste de checkstyle
 *ID       : AM 185248
 *************************************************************
 *Autor    : Thiago Almeida
 *Data     : 06/02/2017
 *Empresa  : Iteris
 *Descrição: Adicionando variáveis de ambiente
 *ID       : 188840
 *************************************************************
 */
package br.com.rede.ke.conciliation.splitter.lambda.handler;

import static net.logstash.logback.argument.StructuredArguments.value;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.lambda.runtime.events.S3Event;
import com.amazonaws.services.s3.event.S3EventNotification;

import br.com.rede.ke.conciliation.splitter.config.ConfigEnvironment;
import br.com.rede.ke.conciliation.splitter.config.ConfigEnvironmentProperties;
import br.com.rede.ke.conciliation.splitter.exception.ConciliationSplitterEngineException;
import br.com.rede.ke.conciliation.splitter.log.TagEnum;
import br.com.rede.ke.conciliation.splitter.main.SplitterOrchestrator;
import br.com.rede.ke.conciliation.util.enums.LogSource;
import br.com.rede.ke.conciliation.util.exception.ConciliationRuntimeException;
import br.com.rede.ke.conciliation.util.filerepository.FileRepository;
import br.com.rede.ke.conciliation.util.filerepository.FileRepositoryS3;
import br.com.rede.ke.conciliation.util.filerepository.S3File;
import br.com.rede.ke.conciliation.util.log.LogProcessingBuilder;
import br.com.rede.ke.conciliation.util.log.LogProcessingBuilder.LogStatus;
import net.logstash.logback.argument.StructuredArgument;

/**
 * The Class SplitterHandler.
 */
public class SplitterHandler implements RequestHandler<S3Event, Object> {
    /** The Constant LOGGER. */
    private static final Logger KIBANA_LOGGER = LoggerFactory.getLogger(SplitterHandler.class);

    /*
     * (non-Javadoc)
     *
     * @see
     * com.amazonaws.services.lambda.runtime.RequestHandler#handleRequest(java.
     * lang.Object, com.amazonaws.services.lambda.runtime.Context)
     */
    @Override
    public Object handleRequest(S3Event input, Context context) {
        String s3FileKey = null;
        LogProcessingBuilder logBuilder = new LogProcessingBuilder();
        StringBuilder returnMessage = new StringBuilder();
        try {
            ConfigEnvironment configRepository = ConfigEnvironmentProperties.getInstance();
            FileRepository fileRepository = FileRepositoryS3.getInstance();

            SplitterOrchestrator splitterOrchestrator = new SplitterOrchestrator(fileRepository, configRepository);
            List<S3EventNotification.S3EventNotificationRecord> s3EventNotificationRecords = input.getRecords();

            for (S3EventNotification.S3EventNotificationRecord s3EventNotificationRecord : s3EventNotificationRecords) {
                s3EventNotificationRecord.getAwsRegion();
                S3EventNotification.S3Entity s3entity = s3EventNotificationRecord.getS3();

                s3FileKey = s3entity.getObject().getKey();
                String s3Bucket = s3entity.getBucket().getName();

                S3File file = new S3File(s3FileKey);
                file.setBucket(s3Bucket);
                file.setKey(s3FileKey);
                file.setTargetBucket(configRepository.getProperty("BUCKET_PROCESSED"));

                boolean status = false;

                try {
                    logBuilder.addSource(LogSource.SPLITTER);
                    logBuilder.description(String.format("Input file: %s", file.getKey()));

                    splitterOrchestrator.processFile(s3FileKey, s3Bucket, logBuilder);

                    status = true;

                    returnMessage.append("File: ").append(s3FileKey).append(" Status: PROCESSED");
                } catch (ConciliationSplitterEngineException e) {
                    status = false;

                    returnMessage.append("File: ").append(s3FileKey).append("- Status: ERROR - Trace: ")
                    .append(e.getMessage());

                    logBuilder.problem(returnMessage.toString(), new Throwable(e));
                }

                try {
                    // Move o arquivo para o bucket/folder de acordo com o status do
                    // processamento
                    fileRepository.moveProcessed(status, file);
                } catch (ConciliationRuntimeException e) {
                    logBuilder.problem("Error moving file to processed bucket.", (Throwable) e);
                    returnMessage.append("\nError moving file to splitted bucket. Keeping it on its original place.");
                }
            }

        } catch (ConciliationRuntimeException e){
            logBuilder.problem(e.getMessage(), e);
            logBuilder.status(LogStatus.ERROR);
        } finally {
            String fileName = "";
            if(s3FileKey != null){
                fileName = s3FileKey.toString().trim();
            }
            List<StructuredArgument> values = prepareKibanaLogger(fileName, logBuilder);
            if(logBuilder.getStatus() == LogStatus.SUCCESS){
                KIBANA_LOGGER.info("SPLITTER {}", values.toArray());

            }else{
                values.add(value(TagEnum.ERROR_DETAIL.name(), logBuilder.getLastException().toString()));
                KIBANA_LOGGER.info("SPLITTER {}", values.toArray());
            }
        }
        return returnMessage;
    }

    /**
     * Prepare kibana logger.
     *
     * @param fileName the file name
     * @param logBuilder the log builder
     * @return the list
     */
    private List<StructuredArgument> prepareKibanaLogger(String fileName, LogProcessingBuilder logBuilder) {
        List<StructuredArgument> values = new ArrayList<>();
        values.add(value(TagEnum.STATUS.name(), logBuilder.getStatus()));
        values.add(value(TagEnum.SPLITED_FILES_QUANTITY.name(), logBuilder.getSplitedFilesQuantity()));
        values.add(value(TagEnum.FILE_TYPE_NAME.name(), logBuilder.getFileTypeName()));
        values.add(value(TagEnum.FILE_NAME_INPUT.name(), fileName));
        values.add(value(TagEnum.FILE_NAME_OUTPUT.name(), logBuilder.getGenerateFilesNames().toArray()));
        return values;
    }

}