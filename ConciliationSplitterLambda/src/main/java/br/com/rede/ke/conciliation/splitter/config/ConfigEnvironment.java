/*
 * Copyright 2017 Rede S.A.
 *************************************************************
 * Nome     : ConfigEnvironment.java
 * Descrição: ConfigEnvironment.java.
 * Autor    : Thiago Boldrin de Almeida <thiago.almeida@iteris.com.br>
 * Data     : 07/02/2017
 * Empresa  : Iteris
 */
package br.com.rede.ke.conciliation.splitter.config;

/**
 * Created by gustavo.silva on 03/08/2016.
 */
public interface ConfigEnvironment {
    String getProperty(String key);
}
