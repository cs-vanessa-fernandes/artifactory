/*
 * Copyright 2016 Rede S.A.
 *************************************************************
 * Nome     : ConciliationSplitterEngineException.java
 * Descrição: ConciliationSplitterEngineException.java.
 * Autor    : Jefferson Brito <jose.josue@userede.com.br>
 * Data     : 16/11/2016
 * Empresa  : Rede
 */

package br.com.rede.ke.conciliation.splitter.exception;

/**
 * The Class ConciliationSplitterEngineException.
 *
 * @author Symon
 */
public class ConciliationSplitterEngineException extends Exception {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -3118422945651424673L;

    /**
     * Instantiates a new conciliation splitter engine exception.
     *
     * @param messageError
     *            the message error
     */
    public ConciliationSplitterEngineException(String messageError) {
        super(messageError);
    }

    /**
     * Instantiates a new conciliation splitter engine exception.
     *
     * @param e
     *            the e
     */
    public ConciliationSplitterEngineException(Throwable e) {
        super(e);
    }

    /**
     * Instantiates a new conciliation splitter engine exception.
     *
     * @param message
     *            the message
     * @param cause
     *            the cause
     */
    public ConciliationSplitterEngineException(String message, Throwable cause) {
        super(message, cause);
    }
}